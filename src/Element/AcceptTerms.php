<?php

namespace Drupal\agorawebform\Element;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkbox;
use Drupal\Core\Url;

/**
 * Provides a webform "Accept terms" element.
 *
 * @FormElement("agorawebform_accept_terms")
 */
class AcceptTerms extends Checkbox {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return parent::getInfo() + [
      '#terms_url' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      return $element['#default_value'] ?? FALSE;
    }
    else {
      return isset($input);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderCheckbox($element) {
    $element = parent::preRenderCheckbox($element);

    if (empty($element['#title'])) {
      $element['#title'] = (string) t('I agree to the {terms of service}.');
    }
    $fallback_url = \Drupal::config('agorawebform.settings')->get('accept_terms_default_url');
    if (empty($fallback_url)) {
      $fallback_url = '/';
    }
    $url = !empty($element['#terms_url']) ? $element['#terms_url'] : $fallback_url;
    $url = UrlHelper::isExternal($url) ? Url::fromUri($url) : Url::fromUserInput($url);
    $href = $url->toString();
    $element['#title'] = str_replace('{', '<a href="' . $href . '" target="_blank" rel="noopener">', $element['#title']);
    $element['#title'] = str_replace('}', '</a>', $element['#title']);

    // Change description to render array.
    if (isset($element['#description'])) {
      $element['#description'] = ['description' => (is_array($element['#description'])) ? $element['#description'] : ['#markup' => $element['#description']]];
      // @todo Do we need to handle the case differently, if the element was already an array before?
      if (!empty($element['#description']['description']['#markup'])) {
        $element['#description']['description']['#markup'] = str_replace('{', '<a href="' . $href . '" target="_blank" rel="noopener">', $element['#description']['description']['#markup']);
        $element['#description']['description']['#markup'] = str_replace('}', '</a>', $element['#description']['description']['#markup']);
      }
    }
    else {
      $element['#description'] = [];
    }

    // Change #type to checkbox so that element is rendered correctly.
    $element['#type'] = 'checkbox';
    $element['#wrapper_attributes']['class'][] = 'form-type-agorawebform-accept-terms';
    $element['#wrapper_attributes']['class'][] = 'js-form-type-agorawebform-accept-terms';

    return $element;
  }

}
