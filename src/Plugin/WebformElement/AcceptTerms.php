<?php

namespace Drupal\agorawebform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Checkbox;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides an 'accept terms' element.
 *
 * @WebformElement(
 *   id = "agorawebform_accept_terms",
 *   default_key = "accept_terms",
 *   label = @Translation("Accept terms"),
 *   description = @Translation("Provides an 'accept terms' element."),
 *   category = @Translation("Advanced elements"),
 * )
 */
class AcceptTerms extends Checkbox {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $terms_url = $this->configFactory->get('agorawebform.settings')->get('accept_terms_default_url');
    if (empty($terms_url)) {
      $terms_url = '/';
    }
    $properties = [
      'title' => $this->t('I agree to the {terms of service}.'),
      'terms_url' => $terms_url,
    ] + parent::getDefaultProperties();
    unset(
      $properties['icheck'],
      $properties['field_prefix'],
      $properties['field_suffix'],
      $properties['description'],
      $properties['description_display'],
      $properties['title_display']
    );
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    // Set default #title.
    if (empty($element['#title'])) {
      $element['#title'] = $this->getDefaultProperty('title');
    }

    // Set default #terms_url.
    if (empty($element['#terms_url'])) {
      $element['#terms_url'] = $this->getDefaultProperty('terms_url');
    }

    // Backup #title and remove curly brackets.
    // Curly brackets are used to add link to #title when it is rendered.
    // @see \Drupal\agorawebform\Element\AcceptTerms::preRenderCheckbox
    $element['#_agorawebform_accept_terms_title'] = $element['#title'];
    $element['#title'] = str_replace(['{', '}'], ['', ''], $element['#title']);

    parent::initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    // Restore #title with curly brackets.
    if (isset($element['#_agorawebform_accept_terms_title'])) {
      $element['#title'] = $element['#_agorawebform_accept_terms_title'];
      unset($element['#_agorawebform_accept_terms_title']);
    }

    parent::prepare($element, $webform_submission);
  }

  /**
   * {@inheritdoc}
   */
  public function preview() {
    return [
      '#type' => $this->getTypeName(),
      '#title' => $this->t('I agree to the {terms of service}.'),
      '#required' => TRUE,
      '#terms_url' => $this->getDefaultProperty('terms_url'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#description'] = $this->t('In order to create a link to your terms, wrap the words where you want your link to be in curly brackets.');

    $form['terms_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL of the terms of service page.'),
      '#description' => $this->t('Internal paths will be correctly translated. Leave blank to use site default setting.'),
    ];
    return $form;
  }

}
