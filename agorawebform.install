<?php

/**
 * @file
 * Install, update and uninstall functions for the agorawebform module.
 */

/**
 * Implements hook_install().
 */
function agorawebform_install() {
  $moderator_role = \Drupal::moduleHandler()->moduleExists('agorabase') ? agorabase_get_moderator_role() : '';
  if (empty($moderator_role)) {
    $moderator_role = 'presenter';
  }

  user_role_grant_permissions($moderator_role, [
    'access webform overview',
    'view any webform submission',
  ]);

  // Do not allow posting from a dedicated url by default for all webforms.
  \Drupal::configFactory()->getEditable('webform.settings')
    ->set('settings.default_page', FALSE)
    ->save();

  // Disable all webform libraries by default. We do this most of the time!
  agorawebform_disable_webform_libraries();
}

/**
 * Set default value for new 'accept_terms_default_url' setting.
 */
function agorawebform_update_8101() {
  \Drupal::configFactory()->getEditable('agorawebform.settings')->set('accept_terms_default_url', '/')->save();
}
